<?php
$menuItems = [
    "Gast" => [
        array('login', 'Inloggen', 'R')
    ],

    "Leraar" => [
        array('logout', 'Uitloggen', 'R'),
        array('leraarHome', 'Home', 'L'),
        array('klassen', 'Klassen', 'L'),
        array('leerlingen', 'Leerlingen', 'L'),
        array('niveaus', 'Niveaus', 'L'),
        array('strategieën', 'Strategieën', 'L'),
        array('bewerkingen', 'Bewerkingen', 'L'),
        array('sommen', 'Sommen', 'L'),
        array('leerlingenOverzicht', 'Overzicht', 'L'),
    ],

    "Admin" => [
        array('logout', 'Uitloggen', 'R'),
        array('home', 'Home', 'L'),
        array('leraren', 'Leraren', 'L')
    ]
];
