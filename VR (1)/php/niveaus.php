<?php
require "dbh.php";
session_start();

if ($_POST['action'] === 'delete') {
    $deleteNiveau = $conn->prepare("DELETE from niveaus WHERE id=:id");
    $deleteNiveau->execute(array(
        ":id" => $_POST['id']
    ));
    $_SESSION['successMessage'] = "Het niveau is succesvol verwijderd";
    echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=niveaus");</script>';

} else if ($_POST['action'] === 'add') {
    $niveauIdCheck = $conn->prepare("SELECT NULL FROM niveaus WHERE naam=:naam");
    $niveauIdCheck->execute(array(
        ":naam" => $_POST['naam']
    ));

    if ($niveauIdCheck->rowCount() === 0) {
        $addBewerking = $conn->prepare("INSERT INTO niveaus (naam) 
                                               VALUES (:naam)");
        $addBewerking->execute(array(
            ":naam" => $_POST['naam'],
        ));

        $_SESSION['successMessage'] = "Het niveau is succesvol toegevoegd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=niveaus");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een niveau met deze naam";
    }
} else if ($_POST['action'] === 'edit') {
    $niveauIdCheck = $conn->prepare("SELECT NULL FROM niveaus WHERE naam=:naam
                                                    AND id!=:id");
    $niveauIdCheck->execute(array(
        ":naam" => $_POST['naam'],
        ":id" => $_POST['id']
    ));

    if ($niveauIdCheck->rowCount() === 0) {
        $updateBewerking = $conn->prepare("UPDATE niveaus SET 
                                                  naam=:naam
                                                  WHERE id=:id");
        $updateBewerking->execute(array(
            ":naam" => $_POST['naam'],
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De gegevens van het niveau zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=niveaus");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een niveau met deze naam";
    }
}
echo '<script>window.history.go(-1);</script>';