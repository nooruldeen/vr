<?php
require "dbh.php";
session_start();

if ($_POST['action'] === 'delete') {
    $deleteBewerking = $conn->prepare("DELETE from bewerkingen WHERE id=:id");
    $deleteBewerking->execute(array(
        ":id" => $_POST['id']
    ));
    $_SESSION['successMessage'] = "De bewerking is succesvol verwijderd";
    echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=bewerkingen");</script>';

} else if ($_POST['action'] === 'add') {
    $bewerkindIdCheck = $conn->prepare("SELECT NULL FROM bewerkingen WHERE naam=:naam");
    $bewerkindIdCheck->execute(array(
        ":naam" => $_POST['naam']
    ));

    if ($bewerkindIdCheck->rowCount() === 0) {
        $addBewerking = $conn->prepare("INSERT INTO bewerkingen (naam) 
                                               VALUES (:naam)");
        $addBewerking->execute(array(
            ":naam" => $_POST['naam'],
        ));

        $_SESSION['successMessage'] = "De bewerking is succesvol toegevoegd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=bewerkingen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een bewerking met deze naam";
    }
} else if ($_POST['action'] === 'edit') {
    $bewerkindIdCheck = $conn->prepare("SELECT NULL FROM bewerkingen WHERE naam=:naam
                                                    AND id!=:id");
    $bewerkindIdCheck->execute(array(
        ":naam" => $_POST['naam'],
        ":id" => $_POST['id']
    ));

    if ($bewerkindIdCheck->rowCount() === 0) {
        $updateBewerking = $conn->prepare("UPDATE bewerkingen SET 
                                                  naam=:naam
                                                  WHERE id=:id");
        $updateBewerking->execute(array(
            ":naam" => $_POST['naam'],
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De gegevens van de bewerking zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=bewerkingen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een bewerking met deze naam";
    }
}
echo '<script>window.history.go(-1);</script>';