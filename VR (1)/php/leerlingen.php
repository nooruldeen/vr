<?php
require "dbh.php";
session_start();

echo '<pre>', print_r($_POST, true), '</pre>';
if ($_POST['action'] === 'delete') {
    $deleteLeraar = $conn->prepare("DELETE from leerlingen WHERE id=:id");
    $deleteLeraar->execute(array(
        ":id" => $_POST['id']
    ));
    $_SESSION['successMessage'] = "De leerling is succesvol verwijderd";
    echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leerlingen");</script>';

} else if ($_POST['action'] === 'add') {
    $leerlingNummerCheck = $conn->prepare("SELECT NULL FROM leerlingen WHERE leerlingnummer=:leerlingnummer ");
    $leerlingNummerCheck->execute(array(
        ":leerlingnummer" => $_POST['leerlingnummer']
    ));

    if ($leerlingNummerCheck->rowCount() === 0) {
        $addLeraar = $conn->prepare("INSERT INTO leerlingen (leerlingnummer, voornaam, achternaam, klas_id) 
                                              VALUES (:leerlingnummer, :voornaam, :achternaam, :klas)");
        $addLeraar->execute(array(
            ":leerlingnummer" => $_POST['leerlingnummer'],
            ":voornaam" => $_POST['voornaam'],
            ":achternaam" => $_POST['achternaam'],
            ":klas" => $_POST['inputKlas']
        ));

        $_SESSION['successMessage'] = "De leerling is succesvol toegevoegd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leerlingen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een account met dit leerling nummer";
    }
} else if ($_POST['action'] === 'edit') {
    $leerlingNummerCheck = $conn->prepare("SELECT NULL FROM leerlingen WHERE leerlingnummer=:leerlingnummer 
                                                     AND id!=:id");
    $leerlingNummerCheck->execute(array(
        ":leerlingnummer" => $_POST['leerlingnummer'],
        ":id" => $_POST['id']
    ));

    if ($leerlingNummerCheck->rowCount() === 0) {
        $updateLeerling = $conn->prepare("UPDATE leerlingen SET 
                                                  leerlingnummer=:leerlingnummer,
                                                  voornaam=:voornaam,
                                                  achternaam=:achternaam,
                                                  klas_id=:klas 
                                                  WHERE id=:id");
        $updateLeerling->execute(array(
            ":leerlingnummer" => $_POST['leerlingnummer'],
            ":voornaam" => $_POST['voornaam'],
            ":achternaam" => $_POST['achternaam'],
            ":klas" => $_POST['inputKlas'],
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De gegevens van de leerling zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leerlingen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een account met dit leerling nummer";
    }
}
echo '<script>window.history.go(-1);</script>';