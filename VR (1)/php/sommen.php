<?php
require "dbh.php";
session_start();

echo '<pre>', print_r($_POST, true), '</pre>';

if ($_POST['action'] === 'delete') {
    $deleteLeraar = $conn->prepare("DELETE from sommen WHERE id=:id");
    $deleteLeraar->execute(array(
        ":id" => $_POST['id']
    ));
    $_SESSION['successMessage'] = "De som is succesvol verwijderd";
    echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=sommen");</script>';

} else if ($_POST['action'] === 'add') {
    $somCheck = $conn->prepare("SELECT NULL FROM sommen WHERE som=:som AND rekenstrategie_id=:RID");
    $somCheck->execute(array(
        ":som" => $_POST['som'],
        ":RID" => $_POST['inputRID']
    ));

    if ($somCheck->rowCount() === 0) {
        $addLeraar = $conn->prepare("INSERT INTO sommen (som, rekenstrategie_id, bewerking_id, antwoord, niveau_id) 
                                               VALUES (:som, :RID, :BID, :antwoord, :NID)");
        $addLeraar->execute(array(
            ":som" => $_POST['som'],
            ":RID" => $_POST['inputRID'],
            ":BID" => $_POST['inputBID'],
            ":antwoord" => $_POST['antwoord'],
            ":NID" => $_POST['inputNID']
        ));

        $_SESSION['successMessage'] = "De som is succesvol toegevoegd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=sommen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Deze combinatie van som en rekenstrategie bestaat al";
    }
} else if ($_POST['action'] === 'edit') {
    $somCheck = $conn->prepare("SELECT NULL FROM sommen WHERE som=:som AND rekenstrategie_id=:RID
                                                    AND id!=:id");
    $somCheck->execute(array(
        ":som" => $_POST['som'],
        ":RID" => $_POST['inputRID'],
        ":id" => $_POST['id']
    ));

    if ($somCheck->rowCount() === 0) {
        $updateSom = $conn->prepare("UPDATE sommen SET
                                                  som=:som,
                                                  rekenstrategie_id=:RID,
                                                  bewerking_id=:BID,
                                                  antwoord=:antwoord,
                                                  niveau_id=:NID
                                                  WHERE id=:id");
        $updateSom->execute(array(
            ":som" => $_POST['som'],
            ":RID" => $_POST['inputRID'],
            ":BID" => $_POST['inputBID'],
            ":antwoord" => $_POST['antwoord'],
            ":id" => $_POST['id'],
            ":NID" => $_POST['inputNID']
        ));
        $_SESSION['successMessage'] = "De gegevens van de som zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=sommen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Deze combinatie van som en rekenstrategie bestaat al";
    }
}
echo '<script>window.history.go(-1);</script>';