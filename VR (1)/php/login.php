<?php
require "dbh.php";
session_start();

$badPattern = '/[\<\>\/\'\"\$`]/';

$wachtwoord = $_POST['wachtwoord'];
$email = $_POST['email'];

if (preg_match($badPattern, $email) || preg_match($badPattern, $wachtwoord)) {
    $_SESSION['errorMessage'] = 'Een of meerdere ingevulde karakters zijn niet toegestaan';
    echo '<script>window.history.go(-1);</script>';
} else {

    $getUser = $conn->prepare('SELECT * FROM leraren WHERE email=:email');
    $getUser->execute(array(
        ":email" => $email
    ));

    $result = $getUser->fetch(PDO::FETCH_ASSOC);

    if ($result == '') {
        $getUser = $conn->prepare('SELECT * FROM hoofdaccount WHERE email=:email');
        $getUser->execute(array(
            ":email" => $email
        ));
        $result = $getUser->fetch(PDO::FETCH_ASSOC);
        $resultRol = 'Admin';
    } else {
        $resultRol = 'Leraar';
    }

    $hashedPassword = $result['wachtwoord'];

    if (password_verify($wachtwoord, $hashedPassword)) {
        $_SESSION['successMessage'] = 'U bent ingelogd';
        $_SESSION['rol'] = $resultRol;
        if ($resultRol === 'Leraar') {
            header("Location: /VR_Live_Omgeving/index.php?page=leraarHome");
        } else {
            header("Location: /VR_Live_Omgeving/index.php?page=adminHome");
        }
    } else {
        $_SESSION['errorMessage'] = 'De combinatie email en wachtwoord is incorrect.';
        echo '<script>window.history.go(-1);</script>';
    }
}