<?php
require "dbh.php";
session_start();

echo '<pre>', print_r($_POST, true), '</pre>';
if ($_POST['action'] === 'delete') {
    $deleteLeraar = $conn->prepare("DELETE from leraren WHERE id=:id");
    $deleteLeraar->execute(array(
        ":id" => $_POST['id']
    ));
    $_SESSION['successMessage'] = "De leraar is succesvol verwijderd";
    echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leraren");</script>';


} else if ($_POST['action'] === 'add') {
    $emailCheck = $conn->prepare("SELECT NULL FROM leraren WHERE email=:email ");
    $emailCheck->execute(array(
        ":email" => $_POST['email']
    ));

    if ($emailCheck->rowCount() === 0) {
        if ($_POST['wachtwoord'] === $_POST['wachtwoordHerhalen']) {
            $wachtwoord = $_POST['wachtwoord'];
            $pattern = '/[~\!@#%\^&\*\(\)_\-\+=\{\}\[\]\|;:,\.\?]/';
            $badPattern = '/[\<\>\/\'\"\$]/';
            if (!preg_match($badPattern, $wachtwoord)) {
                if (strlen($wachtwoord) >= 8 && strlen($wachtwoord) <= 35) {
                    if (preg_match('/[A-Z]/', $wachtwoord) &&
                        preg_match('/[a-z]/', $wachtwoord) &&
                        preg_match('/[0-9]/', $wachtwoord) &&
                        preg_match($pattern, $wachtwoord)) {
                        $options = [
                            'cost' => 11,
                        ];
                        $hash = password_hash($wachtwoord, PASSWORD_BCRYPT, $options);

                        $addLeraar = $conn->prepare("INSERT INTO leraren (email, wachtwoord, voornaam, achternaam) 
                                               VALUES (:email, :wachtwoord, :voornaam, :achternaam)");
                        $addLeraar->execute(array(
                            ":email" => $_POST['email'],
                            ":wachtwoord" => $hash,
                            ":voornaam" => $_POST['voornaam'],
                            ":achternaam" => $_POST['achternaam']
                        ));

                        $_SESSION['successMessage'] = "De leraar is succesvol toegevoegd";
                        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leraren");</script>';
                    } else {
                        $_SESSION['errorMessage'] = 'Het wachtwoord moet een letter, hoofdletter, cijfer en speciaal teken bevatten';
                    }
                } else {
                    $_SESSION['errorMessage'] = 'Het wachtwoord moet minstens 8 en maximaal 35 karakters lang zijn';
                }
            } else {
                $_SESSION['errorMessage'] = 'Één van de ingevulde karakters is niet toegestaan';
            }
        } else {
            $_SESSION['errorMessage'] = "De wachtwoorden zijn niet gelijk";
        }
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een account met deze email";
    }
} else if ($_POST['action'] === 'edit') {
    $emailCheck = $conn->prepare("SELECT NULL FROM leraren WHERE email=:email AND id!=:id ");
    $emailCheck->execute(array(
        ":email" => $_POST['email'],
        ":id" => $_POST['id']
    ));

    if ($emailCheck->rowCount() === 0) {
        if ($_POST['wachtwoord'] !== '') {
            if ($_POST['wachtwoord'] === $_POST['wachtwoordHerhalen']) {

                $wachtwoord = $_POST['wachtwoord'];
                $pattern = '/[~\!@#%\^&\*\(\)_\-\+=\{\}\[\]\|;:,\.\?]/';
                $badPattern = '/[\<\>\/\'\"\$]/';
                if (!preg_match($badPattern, $wachtwoord)) {
                    if (strlen($wachtwoord) >= 8 && strlen($wachtwoord) <= 35) {
                        if (preg_match('/[A-Z]/', $wachtwoord) &&
                            preg_match('/[a-z]/', $wachtwoord) &&
                            preg_match('/[0-9]/', $wachtwoord) &&
                            preg_match($pattern, $wachtwoord)) {
                            $options = [
                                'cost' => 11,
                            ];
                            $hash = password_hash($wachtwoord, PASSWORD_BCRYPT, $options);
                        } else {
                            $_SESSION['errorMessage'] = 'Het wachtwoord moet een letter, hoofdletter, cijfer en speciaal teken bevatten';
                        }
                    } else {
                        $_SESSION['errorMessage'] = "Het wachtwoord moet misntens 8 en maximaal 35 karakters lang zijn";
                    }
                } else {
                    $_SESSION['errorMessage'] = "Één van de ingevulde karakters is niet toegestaan";
                    $getPassword = $conn->prepare("SELECT wachtwoord FROM leraren WHERE email=:email");
                    $getPassword->execute(array(
                        ":email" => $_POST['email']
                    ));
                    $data = $getPassword->fetch();
                    $hash = $data[0];
                }
            } else {
                $_SESSION['errorMessage'] = "De ingevulde wachtwoorden zijn niet gelijk";
            }
        } else {
            $getPassword = $conn->prepare("SELECT wachtwoord FROM leraren WHERE email=:email");
            $getPassword->execute(array(
                ":email" => $_POST['email']
            ));
            $data = $getPassword->fetch();
            $hash = $data[0];
        }

        $updateLeraar = $conn->prepare("UPDATE leraren SET 
                                                  email=:email,
                                                  wachtwoord=:wachtwoord,
                                                  voornaam=:voornaam,
                                                  achternaam=:achternaam 
                                                  WHERE id=:id");
        $updateLeraar->execute(array(
            ":email" => $_POST['email'],
            ":wachtwoord" => $hash,
            ":voornaam" => $_POST['voornaam'],
            ":achternaam" => $_POST['achternaam'],
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De gegevens van de leraar zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leraren");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een account met deze email";
    }
}
echo '<script>window.history.go(-1);</script>';