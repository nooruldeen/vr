<?php
$servername = "localhost";
$username = "root";
$password = "";

$conn = NULL;

try {
    $conn = new PDO("mysql:host=$servername; dbname=liveomgeving_vr", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}


