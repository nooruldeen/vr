<?php
require "dbh.php";
session_start();

if ($_POST['action'] === 'delete') {
    $klas_id = $_POST['id'];
    $checkLeerlingenInKlas = $conn->prepare("SELECT NULL FROM leerlingen WHERE klas_id=:klas_id");
    $checkLeerlingenInKlas->execute(array(
        ":klas_id" => $klas_id
    ));

    if ($checkLeerlingenInKlas->rowCount() > 0) {
        $_SESSION['errorMessage'] = "Er zitten nog leerlingen in deze klas";

    } else {
        $deleteKlas = $conn->prepare("DELETE from klassen WHERE id=:id");
        $deleteKlas->execute(array(
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De klas is succesvol verwijderd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=klassen");</script>';
    }
} else if ($_POST['action'] === 'add') {
    $klasIdCheck = $conn->prepare("SELECT NULL FROM klassen WHERE naam=:naam");
    $klasIdCheck->execute(array(
        ":naam" => $_POST['naam']
    ));

    if ($klasIdCheck->rowCount() === 0) {
        $addKlas = $conn->prepare("INSERT INTO klassen (naam) 
                                            VALUES (:naam)");
        $addKlas->execute(array(
            ":naam" => $_POST['naam'],
        ));

        $_SESSION['successMessage'] = "De klas is succesvol toegevoegd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=klassen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een klas met deze naam";
    }
} else if ($_POST['action'] === 'edit') {
    $klasIdCheck = $conn->prepare("SELECT NULL FROM klassen WHERE naam=:naam
                                                    AND id!=:id");
    $klasIdCheck->execute(array(
        ":naam" => $_POST['naam'],
        ":id" => $_POST['id']
    ));

    if ($klasIdCheck->rowCount() === 0) {
        $updateLeerling = $conn->prepare("UPDATE klassen SET 
                                                  naam=:naam
                                                  WHERE id=:id");
        $updateLeerling->execute(array(
            ":naam" => $_POST['naam'],
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De gegevens van de klassen zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=klassen");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een klas met deze naam";
    }
}
echo '<script>window.history.go(-1);</script>';