<?php
require "dbh.php";
session_start();

if (isset($_POST['actief'])) {
    $actief = 1;
} else {
    $actief = 0;
}

if ($_POST['action'] === 'delete') {
    $deleteLeraar = $conn->prepare("DELETE from rekenstrategieen WHERE id=:id");
    $deleteLeraar->execute(array(
        ":id" => $_POST['id']
    ));
    $_SESSION['successMessage'] = "De rekenstrategie is succesvol verwijderd";
    echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=strategieën");</script>';

} else if ($_POST['action'] === 'add') {
    $leerlingNummerCheck = $conn->prepare("SELECT NULL FROM rekenstrategieen WHERE naam=:naam");
    $leerlingNummerCheck->execute(array(
        ":naam" => $_POST['naam']
    ));

    if ($leerlingNummerCheck->rowCount() === 0) {
        $addLeraar = $conn->prepare("INSERT INTO rekenstrategieen (naam, actief) 
                                               VALUES (:naam, :actief)");
        $addLeraar->execute(array(
            ":naam" => $_POST['naam'],
            ":actief" => $actief
        ));

        $_SESSION['successMessage'] = "De rekenstrategie is succesvol toegevoegd";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=strategieën");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een rekenstrategie met deze naam";
    }
} else if ($_POST['action'] === 'edit') {
    $leerlingNummerCheck = $conn->prepare("SELECT NULL FROM rekenstrategieen WHERE naam=:naam
                                                    AND id!=:id");
    $leerlingNummerCheck->execute(array(
        ":naam" => $_POST['naam'],
        ":id" => $_POST['id']
    ));

    if ($leerlingNummerCheck->rowCount() === 0) {
        $updateLeerling = $conn->prepare("UPDATE rekenstrategieen SET 
                                                  naam=:naam,
                                                  actief=:actief 
                                                  WHERE id=:id");
        $updateLeerling->execute(array(
            ":naam" => $_POST['naam'],
            ":actief" => $actief,
            ":id" => $_POST['id']
        ));
        $_SESSION['successMessage'] = "De gegevens van de rekenstrategie zijn aangepast";
        echo '<script>window.location.replace("http://localhost/VR_Live_Omgeving/index.php?page=strategieën");</script>';
    } else {
        $_SESSION['errorMessage'] = "Er bestaat al een rekenstrategie met deze naam";
    }
}
echo '<script>window.history.go(-1);</script>';