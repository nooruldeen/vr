<?php
require "php/dbh.php";

if (isset($_SESSION['successMessage'])) {
    echo '<div id="toastr" class="hide success"><strong>' . $_SESSION['successMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['successMessage']);
}

if (isset($_SESSION['errorMessage'])) {
    echo '<div id="toastr" class="hide"><strong>' . $_SESSION['errorMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['errorMessage']);
}

//If a row in the table is selected this gets the value
$selectedRow = 0;
if (isset($_GET['val'])) {
    $selectedRow = $_GET['val'];
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-7">
            <table>
                <thead>
                <tr>
                    <th class="tb-w-20">ID</th>
                    <th class="tb-w-40">Naam</th>
                    <th class="tb-w-20">Actief</th>
                    <th class="tb-w-20">Verwijderen</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getStrategieen = $conn->prepare("SELECT * FROM rekenstrategieen");
                $getStrategieen->execute();
                while ($result = $getStrategieen->fetch()) {
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-20">' . $result['id'] . ' </td>';
                    echo '<td class="tb-w-40">' . $result['naam'] . '</td>';
                    if($result['actief'] === '1'){
                        $actief = 'ja';
                    } else {
                        $actief = 'nee';
                    }
                    echo '<td class="tb-w-20">' . $actief . '</td>';
                    echo '<td class="tb-w-20">
                        <form method="post" class="wrapper" action="php/strategieën.php">
                            <input type="hidden" name="id" value="' . $result['id'] . '">
                            <input type="submit" class="danger" name="submit" value="Delete"
                            onclick="return confirm(`Weet U zeker dat U de rekenstrategie wilt verwijderen?`);">
                            <input type="hidden" name="action" value="delete">
                        </form> 
                    </td>
                </tr>';
                }

                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <?php
            if ($selectedRow !== 0) {
                $getStrategie = $conn->prepare("SELECT id, naam, actief
                                                       FROM rekenstrategieen
                                                       WHERE id=:selectedRow");
                $getStrategie->execute(array(
                    ":selectedRow" => $selectedRow
                ));
                $result = $getStrategie->fetch();
            } ?>

            <form class="strategieën-form" method="post" action="php/strategieën.php">
                <div class="form-group">
                    <label for="inputNaam">Strategie</label>
                    <input type="text" class="form-control darkPlaceholder" id="inputNaam"
                           placeholder="Naam" name="naam" required
                           value="<?= $result['naam'] ?>">
                </div>
                Actief
                <div class="form-group">
                    <label class="switch">
                        <input type="checkbox" class="form-control"
                               name="actief"
                        <?php if($result['actief'] == 1){
                            ?>checked<?php
                        } ?>>
                        <span class="slider round"></span>
                    </label>
                </div>

                <?php
                if ($selectedRow !== 0) {
                    echo '<input type="hidden" name="action" value="edit">';
                    echo '<input type="hidden" name="id" value="' . $result['id'] . '">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens opslaan</button>';
                } else {
                    echo '<input type="hidden" name="action" value="add">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens toevoegen</button>';
                }
                ?>
            </form>

        </div>
        <div class="col-md-1">
            <a href="http://localhost/VR_Live_Omgeving/index.php?page=strategieën" class="btn btn-primary btn-margin">Velden legen</a>
        </div>
    </div>
</div>

<!--Checks which table row is selected and sends it to the URL-->
<script>
    $(function () {
        let rows = $('tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            location.replace("http://localhost/VR_Live_Omgeving/index.php?page=strategieën&val=" + row[0].firstElementChild.innerText);
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    // Gets the value from the url and highlights the row
    jQuery.each($('tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("val");
        console.log(val);
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");
        }
    });
</script>