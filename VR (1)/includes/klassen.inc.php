<?php
require "php/dbh.php";

if (isset($_SESSION['successMessage'])) {
    echo '<div id="toastr" class="hide success"><strong>' . $_SESSION['successMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['successMessage']);
}

if (isset($_SESSION['errorMessage'])) {
    echo '<div id="toastr" class="hide"><strong>' . $_SESSION['errorMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['errorMessage']);
}

//If a row in the table is selected this gets the value
$selectedRow = 0;
if (isset($_GET['val'])) {
    $selectedRow = $_GET['val'];
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-7">
            <table>
                <thead>
                <tr>
                    <th class="tb-w-33">ID</th>
                    <th class="tb-w-33">Naam</th>
                    <th class="tb-w-33">Verwijderen</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getKlassen = $conn->prepare("SELECT * FROM klassen");
                $getKlassen->execute();
                while ($result = $getKlassen->fetch()) {
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-33">' . $result['id'] . ' </td>';
                    echo '<td class="tb-w-33">' . $result['naam'] . '</td>';
                    echo '<td class="tb-w-33">
                        <form method="post" class="wrapper" action="php/klassen.php">
                            <input type="hidden" name="id" value="' . $result['id'] . '">
                            <input type="submit" class="danger" name="submit" value="Delete"
                            onclick="return confirm(`Weet U zeker dat U de klas wilt verwijderen?`);">
                            <input type="hidden" name="action" value="delete">
                        </form> 
                    </td>
                </tr>';
                }

                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <?php
            if ($selectedRow !== 0) {
                $getKlas = $conn->prepare("SELECT * FROM klassen
                                                       WHERE id=:selectedRow");
                $getKlas->execute(array(
                    ":selectedRow" => $selectedRow
                ));
                $result = $getKlas->fetch();
            } ?>

            <form class="klassen-form" method="post" action="php/klassen.php">
                <div class="form-group">
                    <label for="inputNaam">Naam</label>
                    <input type="text" class="form-control darkPlaceholder" id="inputNaam"
                           placeholder="Naam" name="naam" required
                           value="<?= $result['naam'] ?>">
                </div>
                <?php
                if ($selectedRow !== 0) {
                    echo '<input type="hidden" name="action" value="edit">';
                    echo '<input type="hidden" name="id" value="' . $result['id'] . '">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens opslaan</button>';
                } else {
                    echo '<input type="hidden" name="action" value="add">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens toevoegen</button>';
                }
                ?>
            </form>

        </div>
        <div class="col-md-1">
            <a href="http://localhost/VR_Live_Omgeving/index.php?page=klassen" class="btn btn-primary btn-margin">Velden legen</a>
        </div>
    </div>
</div>

<!--Checks which table row is selected and sends it to the URL-->
<script>
    $(function () {
        let rows = $('tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            location.replace("http://localhost/VR_Live_Omgeving/index.php?page=klassen&val=" + row[0].firstElementChild.innerText);
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    // Gets the value from the url and highlights the row
    jQuery.each($('tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("val");
        console.log(val);
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");
        }
    });
</script>