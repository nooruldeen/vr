<?php
if (isset($_SESSION['errorMessage'])) {
        echo '<div id="toastr" class="hide"><strong>' . $_SESSION['errorMessage'] . '</strong></div>';
        ?>
        <script>
            window.onload = function () {
                fancyToaster();
            };
        </script>
        <?php
        unset($_SESSION['errorMessage']);
    }

if(isset($_SESSION['rol']) && $_SESSION['rol'] !== 'Gast'){
    echo 'not allowed';
} else {
?>



<div class="container logincontainer">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h1>Login</h1>
            <form class="inlog-form" method="post" action="php/login.php">
                <div class="form-group">
                    <label for="inputEmail"></label>
                    <input type="text" class="form-control darkPlaceholder" id="inputEmail"
                           aria-describedby="emailHelp" placeholder="Email"
                           name="email" autocomplete="none">
                </div>

                <div class="form-group">
                    <label for="inputPassword"></label>
                    <input type="password" class="form-control darkPlaceholder" id="inputPassword"
                           placeholder="Wachtwoord" name="wachtwoord">
                </div>

                <button type="submit" class="btn btn-primary">Inloggen</button>
            </form>
        </div>
    </div>
</div>

<?php } ?>