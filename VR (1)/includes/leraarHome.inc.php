<?php
include "php/roles.php";

if (isset($_SESSION['successMessage'])) {
    echo '<div id="toastr" class="hide success"><strong>' . $_SESSION['successMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['successMessage']);
}
?>

<div class="container">
    <div class="row">
        <h1>Dashboard Leraren</h1>
    </div>

    <?php
    $count = 0;
    foreach ($menuItems['Leraar'] as $menuItem) {
        if ($count === 0) {
            ?>
            <div class="row pageButtonRow">
            <?php
        }
        if ($menuItem[1] !== 'Home') {
            $count++;
            ?>
            <div class="col-md-4">
                <a href="index.php?page=<?= $menuItem[0]?>" class="btn btn-primary pageButton"><?= $menuItem[1] ?></a>
            </div>
            <?php
        }
        if ($count === 3) {
            ?>
            </div>
            <?php
            $count = 0;
        }
    }
    ?>
</div>
