<?php
require "php/dbh.php";

if (isset($_SESSION['successMessage'])) {
    echo '<div id="toastr" class="hide success"><strong>' . $_SESSION['successMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['successMessage']);
}

if (isset($_SESSION['errorMessage'])) {
    echo '<div id="toastr" class="hide"><strong>' . $_SESSION['errorMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['errorMessage']);
}

//If a row in the table is selected this gets the value
$selectedRow = 0;
if (isset($_GET['val'])) {
    $selectedRow = $_GET['val'];
}

$transformerenNaam = '';
$bewerkingNaam = '';
$niveauNaam = '';
?>

<div class="container">
    <div class="row">
        <div class="col-md-7">
            <table>
                <thead>
                <tr>
                    <th class="tb-w-10">ID</th>
                    <th class="tb-w-20">Rekenstrategie naam</th>
                    <th class="tb-w-20">Bewerking naam</th>
                    <th class="tb-w-15">Som</th>
                    <th class="tb-w-15">Antwoord</th>
                    <th class="tb-w-10">Niveau</th>
                    <th class="tb-w-10">Verwijderen</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getSommen = $conn->prepare("SELECT S.id as somID,
                                                      S.som as som, 
                                                      R.naam as rn, 
                                                      B.naam as bn, 
                                                      S.antwoord as ans,
                                                      N.naam as nn
                                                      FROM sommen AS S
                                                      INNER JOIN rekenstrategieen AS R
                                                      ON S.rekenstrategie_id = R.id
                                                      INNER JOIN bewerkingen AS B
                                                      ON S.bewerking_id = B.id
                                                      INNER JOIN niveaus AS N
                                                      ON S.niveau_id = N.id");
                $getSommen->execute();
                while ($result = $getSommen->fetch()) {
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-10">' . $result['somID'] . ' </td>';
                    echo '<td class="tb-w-20">' . $result['rn'] . '</td>';
                    echo '<td class="tb-w-20">' . $result['bn'] . '</td>';
                    echo '<td class="tb-w-15">' . $result['som'] . '</td>';
                    echo '<td class="tb-w-15">' . $result['ans'] . '</td>';
                    echo '<td class="tb-w-10">' . $result['nn'] . '</td>';
                    echo '<td class="tb-w-10">
                        <form method="post" class="wrapper" action="php/sommen.php">
                            <input type="hidden" name="id" value="' . $result['somID'] . '">
                            <input type="submit" class="danger" name="submit" value="Delete"
                            onclick="return confirm(`Weet U zeker dat U de som wilt verwijderen?`);">
                            <input type="hidden" name="action" value="delete">
                        </form> 
                    </td>
                </tr>';
                }

                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <?php
            if ($selectedRow !== 0) {
                $getSom = $conn->prepare("SELECT S.id as somID,
                                                   S.som as som,
                                                   R.naam as rn, 
                                                   B.naam as bn, 
                                                   S.antwoord as ans,
                                                   N.naam as nn 
                                                   FROM sommen AS S
                                                   INNER JOIN rekenstrategieen AS R
                                                   ON S.rekenstrategie_id = R.id
                                                   INNER JOIN bewerkingen AS B
                                                   ON S.bewerking_id = B.id
                                                   INNER JOIN niveaus AS N
                                                   ON S.niveau_id = N.id
                                                   WHERE s.ID=:selectedRow");
                $getSom->execute(array(
                    ":selectedRow" => $selectedRow
                ));
                $result = $getSom->fetch(PDO::FETCH_ASSOC);
                $transformerenNaam = $result['rn'];
                $bewerkingNaam = $result['bn'];
                $niveauNaam = $result['nn'];

            } ?>
            <form class="sommen-form" method="post" action="php/sommen.php">
                <div class="form-group">
                    <label for="inputRID">Rekenstrategie</label>
                    <select name="inputRID" class="form-control">
                        <?php
                        $getStrategieen = $conn->prepare("SELECT id, naam FROM rekenstrategieen");
                        $getStrategieen->execute();
                        $results = $getStrategieen->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($results as $r) {
                            if ($selectedRow !== 0) {
                                if ($transformerenNaam === $r['naam']) {
                                    echo '<option value="' . $r['id'] . '" selected>' . $r['naam'] . '</option>';
                                } else {
                                    echo '<option value="' . $r['id'] . '">' . $r['naam'] . '</option>';
                                }
                            } else {
                                echo '<option value="' . $r['id'] . '">' . $r['naam'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="inputBID">Bewerking</label>
                    <select name="inputBID" class="form-control">
                        <?php
                        $getBewerkingen = $conn->prepare("SELECT id, naam FROM bewerkingen");
                        $getBewerkingen->execute();
                        $results = $getBewerkingen->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($results as $r) {
                            if ($selectedRow !== 0) {
                                if ($bewerkingNaam === $r['naam']) {
                                    echo '<option value="' . $r['id'] . '" selected>' . $r['naam'] . '</option>';
                                } else {
                                    echo '<option value="' . $r['id'] . '">' . $r['naam'] . '</option>';
                                }
                            } else {
                                echo '<option value="' . $r['id'] . '">' . $r['naam'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputSom">Som</label>
                    <input type="text" class="form-control darkPlaceholder" id="inputSom"
                           placeholder="Som" name="som" required
                           value="<?= $result['som'] ?>">
                </div>
                <div class="form-group">
                    <label for="inputAntwoord">Antwoord</label>
                    <input type="number" class="form-control darkPlaceholder" id="inputAntwoord"
                           placeholder="Antwoord" name="antwoord" required
                           value="<?= $result['ans'] ?>">
                </div>
                <div class="form-group">
                    <label for="inputBID">Niveau</label>
                    <select name="inputNID" class="form-control">
                        <?php
                        $getNiveaus = $conn->prepare("SELECT id, naam FROM niveaus");
                        $getNiveaus->execute();
                        $results = $getNiveaus->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($results as $r) {
                            if ($selectedRow !== 0) {
                                if ($niveauNaam === $r['naam']) {
                                    echo '<option value="' . $r['id'] . '" selected>' . $r['naam'] . '</option>';
                                } else {
                                    echo '<option value="' . $r['id'] . '">' . $r['naam'] . '</option>';
                                }
                            } else {
                                echo '<option value="' . $r['id'] . '">' . $r['naam'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <?php
                if ($selectedRow !== 0) {
                    echo '<input type="hidden" name="action" value="edit">';
                    echo '<input type="hidden" name="id" value="' . $result['somID'] . '">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens opslaan</button>';
                } else {
                    echo '<input type="hidden" name="action" value="add">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens toevoegen</button>';
                }
                ?>
            </form>

        </div>
        <div class="col-md-1">
            <a href="http://localhost/VR_Live_Omgeving/index.php?page=sommen" class="btn btn-primary btn-margin">Velden legen</a>
        </div>
    </div>
</div>

<!--Checks which table row is selected and sends it to the URL-->
<script>
    $(function () {
        let rows = $('tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            location.replace("http://localhost/VR_Live_Omgeving/index.php?page=sommen&val=" + row[0].firstElementChild.innerText);
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    // Gets the value from the url and highlights the row
    jQuery.each($('tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("val");
        console.log(val);
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");
        }
    });
</script>

