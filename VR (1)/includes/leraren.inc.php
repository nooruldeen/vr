<?php
require "php/dbh.php";

if (isset($_SESSION['successMessage'])) {
    echo '<div id="toastr" class="hide success"><strong>' . $_SESSION['successMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['successMessage']);
}

if (isset($_SESSION['errorMessage'])) {
    echo '<div id="toastr" class="hide"><strong>' . $_SESSION['errorMessage'] . '</strong></div>';
    ?>
    <script>
        window.onload = function () {
            fancyToaster();
        };
    </script>
    <?php
    unset($_SESSION['errorMessage']);
}

//If a row in the table is selected this gets the value
$selectedRow = 0;
if (isset($_GET['val'])) {
    $selectedRow = $_GET['val'];
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <table>
                <thead>
                <tr>
                    <th class="tb-w-10">ID</th>
                    <th class="tb-w-30">Email</th>
                    <th class="tb-w-25">Voornaam</th>
                    <th class="tb-w-25">Achternaam</th>
                    <th class="tb-w-10">Verwijderen</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getLeraren = $conn->prepare("SELECT id, email, voornaam, achternaam FROM leraren");
                $getLeraren->execute();
                while ($result = $getLeraren->fetch()) {
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-10">' . $result['id'] . ' </td>';
                    echo '<td class="tb-w-30">' . $result['email'] . '</td>';
                    echo '<td class="tb-w-25">' . $result['voornaam'] . '</td>';
                    echo '<td class="tb-w-25">' . $result['achternaam'] . '</td>';
                    echo '<td class="tb-w-10">
                        <form method="post" class="wrapper" action="php/leraren.php">
                            <input type="hidden" name="id" value="' . $result['id'] . '">
                            <input type="submit" class="danger" name="submit" value="Delete"
                            onclick="return confirm(`Weet U zeker dat U de leraar wilt verwijderen?`);">
                            <input type="hidden" name="action" value="delete">
                        </form> 
                    </td>
                </tr>';
                }

                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <?php
            if ($selectedRow !== 0) {
                $getLeraar = $conn->prepare("SELECT id, email, voornaam, achternaam
                                                       FROM leraren
                                                       WHERE id=:selectedRow");
                $getLeraar->execute(array(
                    ":selectedRow" => $selectedRow
                ));
                $result = $getLeraar->fetch();
            } ?>

            <form class="leraren-form" method="post" action="php/leraren.php">
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input type="email" class="form-control darkPlaceholder" id="inputEmail"
                           placeholder="Email" name="email" required
                           value="<?= $result['email'] ?>">
                </div>
                <div class="form-group">
                    <label for="inputFirstname">Voornaam</label>
                    <input type="text" class="form-control darkPlaceholder" id="inputFirstname"
                           placeholder="Voornaam" name="voornaam" required
                           value="<?= $result['voornaam'] ?>">
                </div>
                <div class="form-group">
                    <label for="inputLastname">Achternaam</label>
                    <input type="text" class="form-control darkPlaceholder" id="inputLastname"
                           placeholder="Achternaam" name="achternaam" required
                           value="<?= $result['achternaam'] ?>">
                </div>
                <div class="form-group">
                    <label for="inputPassword">Wachtwoord</label>
                    <input type="password" class="form-control darkPlaceholder" id="inputPassword"
                           placeholder="Wachtwoord" name="wachtwoord"
                        <?php if ($selectedRow === 0) { ?>
                            required
                        <?php } ?>>
                </div>
                <div class="form-group">
                    <label for="repeatPassword">Wachtwoord herhalen</label>
                    <input type="password" class="form-control darkPlaceholder" id="repeatPassword"
                           placeholder="Wachtwoord Herhalen" name="wachtwoordHerhalen"
                        <?php if ($selectedRow === 0) { ?>
                            required
                        <?php } ?>>
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary" onclick="genpass()">Genereer wachtwoord</button>
                    <button type="button" class="btn btn-primary" onclick="showPassword()">Show password</button>
                </div>


                <?php
                if ($selectedRow !== 0) {
                    echo '<input type="hidden" name="action" value="edit">';
                    echo '<input type="hidden" name="id" value="' . $result['id'] . '">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens opslaan</button>';
                } else {
                    echo '<input type="hidden" name="action" value="add">';
                    echo '<button type="submit" class="btn btn-primary">Gegevens toevoegen</button>';
                }
                ?>
            </form>

        </div>
        <div class="col-md-2">
            <a href="http://localhost/VR_Live_Omgeving/index.php?page=leraren" class="btn btn-primary btn-margin">Velden legen</a>
        </div>
    </div>
</div>

<!--Checks which table row is selected and sends it to the URL-->
<script>
    $(function () {
        let rows = $('tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leraren&val=" + row[0].firstElementChild.innerText);
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    // Gets the value from the url and highlights the row
    jQuery.each($('tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("val");
        console.log(val);
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");
        }
    });

    function showPassword() {
        var x = document.getElementById("inputPassword");
        var x2 = document.getElementById("repeatPassword");
        if (x.type === "password" && x2.type === "password") {
            x.type = "text";
            x2.type = "text";
        } else {
            x.type = "password";
            x2.type = "password";
        }
    }

    function genpass() {
        var randPassword = Array(15).fill("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@#%6&*()_-+={}[]|;:,.?")
            .map(function (x) {
                return x[Math.floor(Math.random() * x.length)]
            }).join('');
        var x = document.getElementById("inputPassword");
        var x2 = document.getElementById("repeatPassword");

        x.value = x2.value = randPassword;
    }

</script>