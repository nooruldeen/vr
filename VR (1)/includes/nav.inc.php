<?php require 'php/roles.php';

if (isset($_SESSION['rol'])) {
    $userArray = $menuItems[$_SESSION["rol"]];
} else {
    $userArray = $menuItems['Gast'];
}
?>

<nav class="navbar navbar-expand-lg navbar-light sticky-top navbar-custom">
    <a class="navbar-brand" href="#"><h3>VR_Live_Omgeving</h3></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php
            foreach ($userArray as $menuItem) {
                if ($menuItem[2] == 'L') {
                    echo '<li class="nav-item">
                              <a class="nav-link" href="index.php?page=' . $menuItem[0] . '">
                                  <h5> ' . $menuItem[1] . '</h5>
                              </a>
                          </li>';
                }
            }
            ?>
        </ul>
        <ul class="navbar-nav navbar-right">
            <?php
            if ($menuItem[1] == "logout") {
                echo '<li class= "nav-item" > <a class="nav-link"href="php/logout.php">' . $menuItem[1] . '</a></li>';
            } else {
                foreach ($userArray as $menuItem) {
                    if ($menuItem[2] == 'R') {
                        echo '<li class="nav-item"><a class="nav-link";    " href="index.php?page=' . $menuItem[0] . '"><h5>' . $menuItem[1] . '</h5></a></li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
</nav>
