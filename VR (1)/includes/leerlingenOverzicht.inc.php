<?php
require "php/dbh.php";

//If a row in the table is selected this gets the value
$selectedRow = 0;
if (isset($_GET['val'])) {
    $selectedRow = $_GET['val'];
}

$selectedKlas = '';
if (isset($_GET['klas'])) {
    $selectedKlas = $_GET['klas'];
}
?>

<div class="customPill">
    <ul>
        <li class="nav-item active">
            <a class="nav-link" href="index.php?page=leerlingenOverzicht">
                <h5>Leerlingen</h5>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?page=strategieënOverzicht">
                <h5>Strategieën</h5>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?page=bewerkingenOverzicht">
                <h5>Bewerkingen</h5>
            </a>
        </li>
    </ul>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="klassen" class="klassen">
                <button id="all" class="btn btn-primary klas-button">Alle Leerlingen</button>
                <?php
                $getKlassen = $conn->prepare("SELECT naam FROM klassen");
                $getKlassen->execute();
                while ($result = $getKlassen->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <button id="<?= $result['naam'] ?>" class="btn btn-primary klas-button"> <?= $result['naam'] ?></button>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <table id="leerlingen" class="smallerTable">
                <thead>
                <tr>
                    <th class="tb-w-10">ID</th>
                    <th class="tb-w-25">Leerling nummer</th>
                    <th class="tb-w-25">Voornaam</th>
                    <th class="tb-w-25">Achternaam</th>
                    <th class="tb-w-15">Klas</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($selectedKlas !== '') {
                    $sql = "SELECT L.id, leerlingnummer, voornaam, achternaam, k.naam FROM leerlingen as L 
                            INNER JOIN klassen as K
                            ON l.klas_id = k.id
                            WHERE k.naam=:klas_naam";
                } else {
                    $sql = "SELECT L.id, leerlingnummer, voornaam, achternaam, k.naam FROM leerlingen as L 
                            LEFT JOIN klassen as K
                            ON l.klas_id = k.id";
                }

                $getLeerlingen = $conn->prepare($sql);
                if ($selectedKlas !== '') {
                    $getLeerlingen->execute(array(
                        ":klas_naam" => $selectedKlas
                    ));
                } else {
                    $getLeerlingen->execute();

                }
                while ($result = $getLeerlingen->fetch()) {
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-10">' . $result['id'] . ' </td>';
                    echo '<td class="tb-w-25">' . $result['leerlingnummer'] . '</td>';
                    echo '<td class="tb-w-25">' . $result['voornaam'] . '</td>';
                    echo '<td class="tb-w-25">' . $result['achternaam'] . '</td>';
                    echo '<td class="tb-w-15">' . $result['naam'] . '</td>
                </tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-7">
            <table class="smallerTable">
                <thead>
                <tr>
                    <th class="tb-w-20">Voornaam</th>
                    <th class="tb-w-20">Achternaam</th>
                    <th class="tb-w-10">Som</th>
                    <th class="tb-w-20">Rekenstrategie</th>
                    <th class="tb-w-10">Bewerking</th>
                    <th class="tb-w-10">Antwoord</th>
                    <th class="tb-w-10">Goed/Fout</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getSommen = $conn->prepare("SELECT LL.voornaam AS vn,
                                                              LL.achternaam AS an,
                                                              S.som,
                                                              R.naam AS rn,
                                                              B.naam AS bn,
                                                              S.antwoord AS aw,
                                                              L.goed_of_fout AS gof
                                                       FROM leerling_som AS L
                                                       INNER JOIN sommen AS S
                                                       ON L.som_id = S.id
                                                       INNER JOIN rekenstrategieen AS R
                                                       ON S.rekenstrategie_id = R.id
                                                       INNER JOIN bewerkingen AS B
                                                       ON S.bewerking_id = B.id
                                                       INNER JOIN leerlingen AS LL
                                                       ON L.leerling_id = LL.id
                                                       WHERE LL.id=:id");
                $getSommen->execute(array(
                    ":id" => $selectedRow
                ));

                while ($result = $getSommen->fetch()) {
                    if ($result['gof'] == 1) {
                        echo '<tr class="correctAnswer">';
                    } else {
                        echo '<tr class="wrongAnswer">';
                    }
                    echo '<td class="tb-w-20">' . $result['vn'] . ' </td>';
                    echo '<td class="tb-w-20">' . $result['an'] . ' </td>';
                    echo '<td class="tb-w-10">' . $result['som'] . ' </td>';
                    echo '<td class="tb-w-20">' . $result['rn'] . ' </td>';
                    echo '<td class="tb-w-10">' . $result['bn'] . ' </td>';
                    echo '<td class="tb-w-10">' . $result['aw'] . ' </td>';
                    echo '<td class="tb-w-10">' . $result['gof'] . ' </td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--Selected person-->
<script>
    window.onload = myMain;

    function myMain() {
        document.getElementById("klassen").onclick = klasSorteer;
    }

    $(function () {
        let rows = $('#leerlingen tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            let url = window.location.href;

            if (url.includes("&klas=") && !url.includes("&val=")) {
                location.replace(url + "&val=" + row[0].firstElementChild.innerText);
            } else if (url.includes("&klas") && url.includes("&val=")) {
                let s = url;
                let newUrl = s.substring(0, s.indexOf('&val='));
                location.replace(newUrl + "&val=" + row[0].firstElementChild.innerText);
            } else {
                location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leerlingenOverzicht&val=" + row[0].firstElementChild.innerText);
            }
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    // Gets the value from the url and highlights the row
    jQuery.each($('#leerlingen tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("val");
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");
        }
    });

    function klasSorteer(e) {
        console.log(e.target.id);
        if (e.target.tagName == 'BUTTON') {
            if (e.target.id == 'all') {
                location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leerlingenOverzicht");
            } else {
                location.replace("http://localhost/VR_Live_Omgeving/index.php?page=leerlingenOverzicht&klas=" + e.target.id);
            }
        }
    }
</script>
