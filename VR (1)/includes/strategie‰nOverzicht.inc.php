<?php
require "php/dbh.php";

//If a row in the table is selected this gets the value
$selectedRow = 0;
if (isset($_GET['val'])) {
    $selectedRow = $_GET['val'];
}

$selectedSom = 0;
if (isset($_GET['strategieënVal'])) {
    $selectedSom = $_GET['strategieënVal'];
}
?>

<div class="customPill">
    <ul>
        <li class="nav-item">
            <a class="nav-link" href="index.php?page=leerlingenOverzicht">
                <h5>Leerlingen</h5>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="index.php?page=strategieënOverzicht">
                <h5>Strategieën</h5>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?page=bewerkingenOverzicht">
                <h5>Bewerkingen</h5>
            </a>
        </li>
    </ul>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <table id="strategieën">
                <thead>
                <tr>
                    <th class="tb-w-20">ID</th>
                    <th class="tb-w-60">Naam</th>
                    <th class="tb-w-20">Actief</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getStrategieen = $conn->prepare("SELECT id, naam, actief FROM rekenstrategieen");
                $getStrategieen->execute();
                while ($result = $getStrategieen->fetch()) {
                    if ($result['actief'] == 1) {
                        $actief = 'Ja';
                    } else {
                        $actief = 'Nee';
                    }
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-20">' . $result['id'] . ' </td>';
                    echo '<td class="tb-w-60">' . $result['naam'] . '</td>';
                    echo '<td class="tb-w-20">' . $actief . '</td>
                </tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table id="strategieSom">
                <thead>
                <tr>
                    <th class="tb-w-25">ID</th>
                    <th class="tb-w-25">Som</th>
                    <th class="tb-w-25">Aantal goed</th>
                    <th class="tb-w-25">Aantal fout</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getSommen = $conn->prepare("SELECT DISTINCT S.id, S.som, LS.goed_of_fout
                                                      FROM sommen AS S
                                                      INNER JOIN leerling_som as LS
                                                      ON S.id = LS.som_id
                                                      WHERE rekenstrategie_id =:id
                                                      ORDER BY id");
                $getSommen->execute(array(
                    ":id" => $selectedRow
                ));

                $goedTotaal = $foutTotaal = 0;
                $results = $getSommen->fetchAll(PDO::FETCH_ASSOC);
                foreach ($results as $key => $result) {
                    $getTotal = $conn->prepare("SELECT COUNT(goed_of_fout) AS c
                                                         FROM leerling_som
                                                         WHERE som_id=:id");

                    $getCorrectTotal = $conn->prepare("SELECT COUNT(goed_of_fout) AS c
                                                                FROM leerling_som
                                                                WHERE som_id=:id
                                                                AND goed_of_fout = 1");
                    $getTotal->execute(array(
                        ":id" => $result['id']
                    ));
                    $getCorrectTotal->execute(array(
                        ":id" => $result['id']
                    ));
                    $total = $getTotal->fetchAll(PDO::FETCH_ASSOC);
                    $correctTotal = $getCorrectTotal->fetchAll(PDO::FETCH_ASSOC);

                    $totalFout = $total[0]['c'] - $correctTotal[0]['c'];
                    $totalGoed = $total[0]['c'] - $totalFout;

                    if ($key + 1 < count($results)) {
                        $curr = $results[$key];
                        $next = $results[$key + 1];
                        if ($curr['id'] !== $next['id']) {
                            echo '<tr>';
                            echo '<td id="id" class="tb-w-25">' . $result['id'] . ' </td>';
                            echo '<td class="tb-w-25">' . $result['som'] . ' </td>';
                            echo '<td class="tb-w-25">' . $totalGoed . ' </td>';
                            echo '<td class="tb-w-25">' . $totalFout . ' </td>';
                            echo '</tr>';
                        }
                    }
                }
                if ($selectedRow != '' && !empty($results)) {
                    $lastItem = end($results);
                    echo '<tr>';
                    echo '<td id="id" class="tb-w-25">' . $lastItem['id'] . ' </td>';
                    echo '<td class="tb-w-25">' . $lastItem['som'] . ' </td>';
                    echo '<td class="tb-w-25">' . $totalGoed . ' </td>';
                    echo '<td class="tb-w-25">' . $totalFout . ' </td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table>
                <thead>
                <tr>
                    <th class="tb-w-33">Voornaam</th>
                    <th class="tb-w-33">Achternaam</th>
                    <th class="tb-w-33">Leerlingnummer</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $getLeerlingen = $conn->prepare("SELECT voornaam, achternaam, leerlingnummer, goed_of_fout
                                                          FROM leerling_som AS LS
                                                          INNER JOIN leerlingen AS L
                                                          ON LS.leerling_id = L.id
                                                          WHERE som_id=:id");
                $getLeerlingen->execute(array(
                    ":id" => $selectedSom
                ));

                while ($result = $getLeerlingen->fetch()) {
                    if ($result['goed_of_fout'] === '1') {
                        echo '<tr class="correctAnswer">';
                    } else {
                        echo '<tr class="wrongAnswer">';
                    }
                    echo '<td class="tb-w-33">' . $result['voornaam'] . ' </td>';
                    echo '<td class="tb-w-33">' . $result['achternaam'] . ' </td>';
                    echo '<td class="tb-w-33">' . $result['leerlingnummer'] . ' </td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--Selected person-->
<script>
    $(function () {
        let rows = $('#strategieën tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            location.replace("http://localhost/VR_Live_Omgeving/index.php?page=strategieënOverzicht&val=" + row[0].firstElementChild.innerText);
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    $(function () {
        let rows = $('#strategieSom tr').not(':first');

        rows.on('click', function (e) {
            let row = $(this);
            let str = window.location.href;
            let pos = str.indexOf("&strategieënVal=");
            let url = str;
            if (pos !== -1) {
                url = str.substring(0, pos);
            }
            location.replace(url + "&strategieënVal=" + row[0].firstElementChild.innerText);
        });

        $(document).bind('selectstart dragstart', function (e) {
            e.preventDefault();
            return false;
        });
    });

    // Gets the value from the url and highlights the row
    jQuery.each($('#strategieën tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("val");
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");
        }
    });
    jQuery.each($('#strategieSom tbody tr #id'), function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        let val = url.searchParams.get("strategieënVal");
        let intval = parseInt(val);
        if (this.textContent == intval) {
            $(this).parent().addClass("highlight");

        }
    });
</script>
