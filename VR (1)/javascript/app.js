function fancyToaster() {
    document.getElementById('toastr').classList.remove('hide');
    document.getElementById('toastr').classList.add('show');
    document.getElementById('toastr').addEventListener('click', removeToastr);
    setTimeout(function () {
        removeToastr();
    }, 5000);

    function removeToastr() {
        document.getElementById('toastr').classList.remove('show');
        document.getElementById('toastr').classList.add('hide');
    }
}